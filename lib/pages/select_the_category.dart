import 'package:fixeditdudes/enter_user_detail.dart';
import 'package:flutter/material.dart';

class SelectTheCategory extends StatefulWidget {
  @override
  _SelectTheCategoryState createState() => _SelectTheCategoryState();
}

class _SelectTheCategoryState extends State<SelectTheCategory> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        iconTheme: IconThemeData(color: Colors.black),
        elevation: 0,
        centerTitle: true,
        title: Text(
          "Select The Category",
          style: TextStyle(color: Colors.black),
        ),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              height: 100,
              width: 100,
              child: ClipOval(
                child: Material(
                  color: Colors.blue, // button color
                  child: InkWell(
                    splashColor: Colors.red, // inkwell color
                    child: Image(
                      image: AssetImage('assets/img/webInterFace.png'),
                    ),
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => EnterUserDetail()));
                    },
                  ),
                ),
              ),
            ),
            Text('Residential'),
            SizedBox(
              height: 100,
            ),
            Container(
              height: 100,
              width: 100,
              child: ClipOval(
                child: Material(
                  color: Colors.blue, // button color
                  child: InkWell(
                    splashColor: Colors.red, // inkwell color
                    child: Image(
                      image: AssetImage('assets/img/webInterFace.png'),
                    ),
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => EnterUserDetail()));
                    },
                  ),
                ),
              ),
            ),
            Text('Commercial'),
          ],
        ),
      ),
    );
  }
}
