import 'package:fixeditdudes/pages/home_page_design.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class HomePageReturn extends StatefulWidget {
  @override
  _HomePageReturnState createState() => _HomePageReturnState();
}

class _HomePageReturnState extends State<HomePageReturn> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // automaticallyImplyLeading: false,
        elevation: 0,
        centerTitle: true,
        backgroundColor: Colors.white,
        title: Text(
          'Services',
          textAlign: TextAlign.left,
          style: TextStyle(
            fontWeight: FontWeight.w700,
            fontSize: 22,
            letterSpacing: 0.2,
            color: Colors.black,
          ),
        ),
      ),
      body: HomePageDesign(),
    );
  }
}
