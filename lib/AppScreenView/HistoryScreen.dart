import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fixeditdudes/Constants/appColors.dart';

class HistoryScreen extends StatefulWidget {
  @override
  _HistoryScreenState createState() => _HistoryScreenState();
}

class _HistoryScreenState extends State<HistoryScreen> {
  List<HistoryData> historyList = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    historyList.add(HistoryData(
        name: "Ac installation",
        time: "3:00 pm",
        price: "500",
        date: "25 March 2021"));

    historyList.add(HistoryData(
        name: "Ac service",
        time: "2:30 pm",
        price: "400",
        date: "28 March 2021"));
    historyList.add(HistoryData(
        name: "Gesar repair",
        time: "4:30 pm",
        price: "200",
        date: "29 March 2021"));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        elevation: 0,
        centerTitle: true,
        backgroundColor: Colors.transparent,
        title: Text(
          'History',
          textAlign: TextAlign.left,
          style: TextStyle(
            fontWeight: FontWeight.w700,
            fontSize: 22,
            letterSpacing: 0.2,
            color: Colors.black,
          ),
        ),
      ),
      body: AnnotatedRegion(
        value: SystemUiOverlayStyle.light,
        child: Container(
          width: double.infinity,
          height: double.infinity,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[Expanded(child: _historyListView())],
          ),
        ),
      ),
    );
  }

  _historyListView() {
    return Container(
        padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
        child: ListView.builder(
          itemCount: historyList.length,
          itemBuilder: (BuildContext context, int index) {
            return _historyListItemView(index);
          },
        ));
  }

  _historyListItemView(index) {
    return Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
        ),
        margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
        padding: EdgeInsets.all(8),
        child: Row(
          children: [
            ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Image.network(
                  "https://picsum.photos/200/300",
                  height: 60,
                  width: 70,
                  fit: BoxFit.cover,
                )),
            SizedBox(
              width: 10,
            ),
            Expanded(
                child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Expanded(
                        child: Text(
                      "${historyList[index].name}",
                      style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.w600,
                          fontSize: 16),
                    )),
                    _textIconWidget(
                      text: "${historyList[index].date}",
                      fontSize: 14,
                      fontWeight: FontWeight.w400,
                    ),
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  children: [
                    Expanded(
                      child: Text("₹ ${historyList[index].price}",
                          style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.w500,
                              fontSize: 15)),
                    ),
                    _textIconWidget(
                        text: "${historyList[index].time}",
                        fontSize: 13,
                        fontWeight: FontWeight.w400),
                  ],
                ),
              ],
            ))
          ],
        ));
  }

  _textIconWidget(
      {IconData icon,
      String text,
      Function onPressed,
      double fontSize,
      Color color,
      FontWeight fontWeight}) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 0),
      child: InkWell(
        onTap: onPressed,
        child: Row(
          children: [
            Text(
              text,
              style: TextStyle(
                  fontSize: fontSize, color: color, fontWeight: fontWeight),
            )
          ],
        ),
      ),
    );
  }
}

class HistoryData {
  String name, date, time, price;

  HistoryData({this.name, this.date, this.time, this.price});
}
