import 'package:fixeditdudes/pages/user_detail_2.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fixeditdudes/Constants/appColors.dart';

class ProfileScreen extends StatefulWidget {
  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        automaticallyImplyLeading: false,
        elevation: 0,
        centerTitle: true,
        backgroundColor: Colors.white,
        title: Text(
          'Profile',
          textAlign: TextAlign.left,
          style: TextStyle(
            fontWeight: FontWeight.w700,
            fontSize: 22,
            letterSpacing: 0.2,
            color: Colors.black,
          ),
        ),
        actions: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: Icon(
              Icons.power_settings_new_sharp,
              color: Colors.black,
            ),
          )
        ],
      ),
      body: AnnotatedRegion(
        value: SystemUiOverlayStyle.light,
        child: Container(
          width: double.infinity,
          height: double.infinity,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Expanded(
                child: Container(
                  child: profileView(),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  profileView() {
    return ListView(
      padding: EdgeInsets.symmetric(horizontal: 22),
      children: [
        SizedBox(
          height: 10,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              "Meet Patel",
              style: TextStyle(
                  fontSize: 25,
                  fontWeight: FontWeight.w700,
                  color: AppColors.appColor),
            ),
            // SizedBox(
            //   width: 60,
            // ),
            IconButton(
              icon: Icon(
                Icons.edit,
              ),
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => EnterUserDetail2()));
              },
            ),
          ],
        ),
        SizedBox(
          height: 30,
        ),
        _textIconWidget(
            text: "About our company", url: "assets/img/building.png"
            // onPressed: () => getData(EditProfile(user: user))
            ),
        SizedBox(
          height: 34,
        ),
        _textIconWidget(
          url: "assets/img/information.png",

          text: "About the app",
          // onPressed: () {
          //   Navigator.push(
          //     context,
          //     MaterialPageRoute(
          //       builder: (context) => ChangePassword(
          //         user: user,
          //       ),
          //     ),
          //   );
          // }
        ),
        SizedBox(
          height: 34,
        ),
        _textIconWidget(
          url: "assets/img/date.png",

          text: "Share",
          // onPressed: () => Navigator.push(
          //   context,
          //   MaterialPageRoute(
          //     builder: (context) => MyBookings(),
          //   ),
          // ),
        ),
        SizedBox(
          height: 34,
        ),
        _textIconWidget(
          url: "assets/img/terms-and-conditions.png",

          text: "Terms and Conditions",
          // onPressed: () => Navigator.push(
          //   context,
          //   MaterialPageRoute(
          //     builder: (context) => Termsandprivacy(
          //       isFromprivacypolicy: false,
          //     ),
          //   ),
          // ),
        ),
        SizedBox(
          height: 34,
        ),
        _textIconWidget(
          url: "assets/img/social-media.png",

          text: "Social media",
          // onPressed: () => Navigator.push(
          //   context,
          //   MaterialPageRoute(
          //     builder: (context) => Termsandprivacy(
          //       isFromprivacypolicy: true,
          //     ),
          //   ),
          // ),
        ),
        SizedBox(
          height: 34,
        ),
        _textIconWidget(
          url: "assets/img/comment.png",

          text: "Feedback",
          // onPressed: () => Navigator.push(
          //   context,
          //   MaterialPageRoute(
          //     builder: (context) => Termsandprivacy(
          //       isFromprivacypolicy: true,
          //     ),
          //   ),
          // ),
        ),
        SizedBox(
          height: 34,
        ),
        Container(
          width: double.infinity,
          height: 1,
          color: Color(0xffBAC0DA),
        ),
        SizedBox(
          height: 32,
        ),
        // _textIconWidget(
        //     text:"Logout",
        //     url:"",
        //     onPressed: () async {
        // // SharedPreferences prefs = await SharedPreferences.getInstance();
        // // prefs.clear();
        // if (mounted)
        //   setState(() {
        //     isLoading = true;
        //   });
        // Map<String, Object> request = new HashMap();
        // CommonResponse registerResponse = CommonResponse.fromJson(
        //   await ApiManager().postCallWithHeader(
        //       AppStrings.LOGOUT_URL, request, context),
        // );
        // SharedPreferences prefs = await SharedPreferences.getInstance();
        // prefs.clear();
        // print(registerResponse.result);
        // if (mounted)
        //   setState(() {
        //     isLoading = false;
        //   });
        // Navigator.pushAndRemoveUntil(
        //     context,
        //     MaterialPageRoute(builder: (context) => LoginScreen()),
        //         (route) => false);
        // AppConstants().showToast(msg: "Logout");
        // }),
      ],
    );
  }

  _textIconWidget({String url, String text, Function onPressed}) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 0),
      child: InkWell(
        onTap: onPressed,
        child: Row(
          children: [
            Container(
                padding: EdgeInsets.all(8),
                decoration: BoxDecoration(
                    color: AppColors.appColor,
                    borderRadius: BorderRadius.circular(10)),
                child: Image.asset(
                  url,
                  height: 22,
                  width: 22,
                  color: Colors.white,
                )),
            SizedBox(
              width: 11.3,
            ),
            Text(text,
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.w400))
          ],
        ),
      ),
    );
  }
}
