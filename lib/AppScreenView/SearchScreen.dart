import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fixeditdudes/Constants/appColors.dart';

class SearchScreen extends StatefulWidget {
  @override
  _SearchScreenState createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  Widget _widget = Container(
    color: Colors.green,
    height: 10,
    width: 10,
  );
  List<String> totalList = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    totalList.add("Air condition");
    totalList.add("Hair cutting");
    totalList.add("Electricians");
    totalList.add("Plumbers");

    _widget = Expanded(
      child: Container(
        color: Colors.white,
        child: _listOfService(),
      ),
    );
  }


  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(
          new FocusNode(),
        );
      },
      child: Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          elevation: 0,
          centerTitle: true,
          backgroundColor: Colors.transparent,
          title: Text(
            'Search for services',
            textAlign: TextAlign.left,
            style: TextStyle(
              fontWeight: FontWeight.w700,
              fontSize: 22,
              letterSpacing: 0.2,
              color: Colors.black,
            ),
          ),
        ),
        body: AnnotatedRegion(
          value: SystemUiOverlayStyle.dark,
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 0, vertical: 20),
            width: double.infinity,
            height: double.infinity,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                _searchTextBar(),
                _widget,
              ],
            ),
          ),
        ),
      ),
    );
  }

  _searchTextBar() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 20),

      child: Card(

        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10),),
        child: Container(
          child: TextFormField(
            onChanged: onChangedOperation,
            decoration: InputDecoration(
              hintText: "Search for services",
              filled: true,
              fillColor: Colors.white,
              border: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.transparent),
                borderRadius: BorderRadius.circular(10),
              ),
              enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.transparent),
                borderRadius: BorderRadius.circular(10),
              ),
              focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.transparent),
                borderRadius: BorderRadius.circular(10),
              ),
            ),
          ),
        ),
      ),
    );
  }

  void onChangedOperation(String text) {
    if (text.toLowerCase() == "ac") {
      _widget = Container(
          // the installation and other widgets are implemented here
          child: _changedItems());
      if (mounted) setState(() {});
    } else {
      _widget = Expanded(
        child: Container(
          color: Colors.white,
          child: _listOfService(),
        ),
      );
      if (mounted) setState(() {});
    }
  }

  _listOfService() {
    return Container(
      padding: EdgeInsets.only(top: 30),
      child: ListView.builder(
        itemCount: totalList.length,
        itemBuilder: (BuildContext context, int index) {
          return Container(
            margin: EdgeInsets.all(10),
            decoration: BoxDecoration(color: Colors.white, boxShadow: [
              BoxShadow(
                color: Colors.black26,
                blurRadius: 5,
                offset: Offset(1, 3),
              )
            ]),
            padding: EdgeInsets.all(20),
            child: Text(
              "${totalList[index]}",
              style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
            ),
          );
        },
      ),
    );
  }

  _changedItems() {
    return Container(
      margin: EdgeInsets.only(right: 10),
      child: Column(
        children: [
          Row(
            children: [
              Expanded(child: _buttonContainer(text: "Service")),
              Expanded(child: _buttonContainer(text: "Repair"))
            ],
          ),
          Row(
            children: [
              Expanded(child: _buttonContainer(text: "Installation")),
              Expanded(child: _buttonContainer(text: "UN & IN")),
            ],
          ),
          _buttonContainer(text: "Any other problem")
        ],
      ),
    );
  }

  _buttonContainer({String text}) {
    return Container(
      margin: EdgeInsets.fromLTRB(10, 10, 0, 5),
      child: MaterialButton(
        shape: RoundedRectangleBorder(
            side: BorderSide(color: Colors.grey[400]),
            borderRadius: BorderRadius.circular(8)),
        padding: EdgeInsets.symmetric(vertical: 18),
        minWidth: MediaQuery.of(context).size.width,
        onPressed: () {},
        child: Text(
          "$text",
          style: TextStyle(
              color: Colors.black, fontSize: 16, fontWeight: FontWeight.w500),
        ),
      ),
    );
  }
}
