import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fixeditdudes/Constants/appColors.dart';
import 'package:fixeditdudes/bottom_nav/bottom_bar_view.dart';
import 'package:fixeditdudes/models/tabIcon_data.dart';
import 'package:fixeditdudes/pages/list_of_products.dart';
import '../Populat_List_file.dart';
import '../course_info_screen.dart';
import '../design_course_app_theme.dart';
import '../fintness_app_theme.dart';
import '../home_design_course.dart';

class MyDiaryScreen extends StatefulWidget {
  const MyDiaryScreen({
    Key key,
    this.animationController,
    String text,
  }) : super(key: key);

  final AnimationController animationController;
  @override
  _MyDiaryScreenState createState() => _MyDiaryScreenState();
}

class _MyDiaryScreenState extends State<MyDiaryScreen>
    with TickerProviderStateMixin {
  CategoryType categoryType = CategoryType.ui;
  AnimationController animationController;

  List<TabIconData> tabIconsList = TabIconData.tabIconsList;
  Widget tabBody = Container(
    color: FitnessAppTheme.background,
  );

  @override
  void initState() {
    tabIconsList.forEach((TabIconData tab) {
      tab.isSelected = false;
    });
    tabIconsList[0].isSelected = true;

    animationController = AnimationController(
        duration: const Duration(milliseconds: 600), vsync: this);
    // tabBody = MyDiaryScreen(
    //   animationController: animationController,
    // );
    super.initState();
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  Future<bool> getData() async {
    await Future<dynamic>.delayed(const Duration(milliseconds: 200));
    return true;
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
    ));
    return Scaffold(
        backgroundColor: Colors.white,

        appBar: AppBar(
        automaticallyImplyLeading: false,
elevation: 0,
        centerTitle: true,
        backgroundColor: Colors.white,
        title:  Text(
        'Services',
        textAlign: TextAlign.left,
        style: TextStyle(
          fontWeight: FontWeight.w700,
          fontSize: 22,
          letterSpacing: 0.2,
          color: Colors.black,

        ),
      ),),
      body:AnnotatedRegion(
      value:SystemUiOverlayStyle.light,
    child: Column(
        children: <Widget>[
          Expanded(
            child: Container(
              child: getPopularCourseUI(),
            ),
          ),
        ],
      ),
    ));
  }

  Widget getPopularCourseUI() {
    return Padding(
      padding: const EdgeInsets.only( left: 18, right: 16),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[

          Expanded(
            child: PopularCourseListView(
              callBack: () {
                moveTo();
              },
            ),
          )
        ],
      ),
    );
  }

  void moveTo() {
    Navigator.push<dynamic>(
      context,
      MaterialPageRoute<dynamic>(
        builder: (BuildContext context) => ListOfProducts(),
      ),
    );
  }

  _gridView(){

  }

  Widget getAppBarUI() {
    return Padding(
      padding: const EdgeInsets.only(top: 8.0, left: 18, right: 18),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            'Services',
            textAlign: TextAlign.left,
            style: TextStyle(
              fontWeight: FontWeight.w700,
              fontSize: 30,
              letterSpacing: 0.2,
              color: Colors.black,

            ),
          ),
        ],
      ),
    );
  }


}
