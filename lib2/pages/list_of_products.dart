import 'package:flutter/material.dart';
import 'package:fixeditdudes/Data/data.dart';
import 'package:fixeditdudes/ui_view/product_list.dart';

class ListOfProducts extends StatefulWidget {
  @override
  _ListOfProductsState createState() => _ListOfProductsState();
}

class _ListOfProductsState extends State<ListOfProducts> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        iconTheme: IconThemeData(color: Colors.black),
        elevation: 0,
        centerTitle: true,
        title: Text(
          "Products",
          style: TextStyle(color: Colors.black),
        ),
      ),
      backgroundColor: Colors.grey[100],
      body: ListView.builder(
          physics: BouncingScrollPhysics(),
          itemCount: details.length,
          itemBuilder: (BuildContext context, int index) {
            return userList(context, index);
          }),
    );
  }
}
