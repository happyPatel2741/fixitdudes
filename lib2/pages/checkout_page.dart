import 'package:flutter/material.dart';
import 'package:fixeditdudes/Constants/appColors.dart';
import 'package:fixeditdudes/bottom_nav/bottomNavigationBarHome.dart';

class CheckoutPage extends StatefulWidget {
  @override
  _CheckoutPageState createState() => _CheckoutPageState();
}

class _CheckoutPageState extends State<CheckoutPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,

      appBar: AppBar(
          backgroundColor: Colors.transparent,
          iconTheme: IconThemeData(color: Colors.black),
          elevation: 0,
          centerTitle: true,
          title: Text(
            "CheckOut",
            style: TextStyle(color: Colors.black),
          )),
      body:_buildBody()
    );
  }
  _buildBody() {
    return Container(
      // decoration: BoxDecoration(gradient: AppColors().homegradient()),
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [Flexible(child: mainBody())],
      ),
    );
  }



  mainBody() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          _headerOfListView(),
          // Padding(
          //   padding: const EdgeInsets.symmetric(horizontal: 12.0, vertical: 8),
          //   child: Divider(),

          // // ),
          // Expanded(
          //   child: cartBodyView(),
          // ),
          // // _subTotalView(),
          Expanded(
            child: SizedBox(
              height: 9,
            ),
          ),
          _totalView(),
          SizedBox(
            height: 17,
          ),
          _bottomButton(),
          SizedBox(
            height: 10,
          )
        ],
      ),
    );
  }

  _iconTextWidget({IconData icon, String text, String url}) {
    return Row(
      children: [
        Icon(
          icon,
          size: 15,
          color: AppColors.appColor,
        ),
        SizedBox(
          width: 5.7,
        ),
        Text(text,
            style: TextStyle(
              fontSize: 12,
            ))
      ],
    );
  }

  String startTime, endTime, bookedDate;

  // _data() {
  //   _startTime = DateFormat("HH:mm a")
  //       .format(DateTime.parse("${widget.bookedData.bookingSlotStartTime}"));
  //   _endTime = DateFormat("HH:mm a")
  //       .format(DateTime.parse("${widget.bookedData.bookingSlotEndTime}"));
  //   print(_startTime + " sdfasdfasdf " + _endTime);
  //   bookedDate = DateFormat("MMMM dd, yyyy")
  //       .format(DateTime.parse("${widget.bookedData.bookingDate}"));
  // }

  _headerOfListView() {
    return Container(
      margin: EdgeInsets.fromLTRB(17, 0, 17, 0),
      child: Container(
        child: Padding(
          padding: EdgeInsets.fromLTRB(0, 14, 0, 10),
          child: Row(
            children: [
  Container(child: ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Container(
                  child: Image.asset(
                    "assets/img/applianceRepairs.png",
                    fit: BoxFit.cover,
                    width: 67,
                    height: 51,
                  ),
                ),
              ),)

,              SizedBox(
                width: 7,
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(left: 17.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Expanded(
                            child: Text(
                              "Ac service & Repair",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 15,
                                  fontWeight: FontWeight.w600),
                            ),
                          ),
                          Icon(
                            Icons.delete_outlined,
                            color: Colors.red,
                          ),
                        ],
                      ),
                      Padding(
                        padding: const EdgeInsets.only(right:40.0),
                        child: Text(
                          "This Service Includes Of Cleaning & Washing Of AC For Indoor And Outdoor Unit.",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 12,
                              fontWeight: FontWeight.w300),
                        ),
                      ),
                      SizedBox(
                        height: 8,
                      ),
                      _iconTextWidget(
                          text: "12:00 pm",
                          icon: Icons.access_time_outlined),
                      SizedBox(
                        height: 8,
                      ),
                      Row(
                        children: [
                          Expanded(
                              child: _iconTextWidget(
                                  text: "12 Jan, 2021",
                                  icon: Icons.calendar_today_outlined,
                                  url: "assets/images/calender.png")),
                          Text(
                            "₹ 230",
                            style: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.w500),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  cartBodyView() {
    return Container(
      margin: EdgeInsets.fromLTRB(17, 0, 17, 10),
      decoration: BoxDecoration(
          border: Border(top: BorderSide(color: Color(0xffECECEC)))),
      child: ListView.builder(
          itemCount:3,
          itemBuilder: (BuildContext context, int index) {
            return _cartBodyItemView(index);
          }),
    );
  }

  _cartBodyItemView( index) {
    return Container(
      margin: EdgeInsets.only(bottom: 0),
      padding: EdgeInsets.fromLTRB(7, 10, 0, 14),
      decoration: BoxDecoration(
          border: Border(bottom: BorderSide(color: Color(0xffECECEC)))),
      child: Row(
        children: [
        ClipRRect(
            borderRadius: BorderRadius.circular(10),
            child: Container(
              child: Image.asset(
                "assets/images/pitchImage.png",
                fit: BoxFit.contain,
                width: 44,
                height: 31,
              ),
            ),
          ),
          //     : ClipRRect(
          //   borderRadius: BorderRadius.circular(10),
          //   child: CachedNetworkImage(
          //     progressIndicatorBuilder:
          //         (context, url, downloadProgress) => SpinKitThreeBounce(
          //       size: 20,
          //       duration: new Duration(milliseconds: 1800),
          //       color: AppColors.appColor,
          //     ),
          //     imageUrl: "assets/img/area1.png",
          //     width: 60,
          //     height: 60,
          //     fit: BoxFit.contain,
          //   ),
          // ),
          SizedBox(
            width: 21,
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Expanded(
                      child: Container(
                        child: Text(
                          "item.title",
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: 15,
                          ),
                        ),
                      ),
                    ),
                    Icon(
                      Icons.delete_outlined,
                      color: Colors.red,
                    ),
                  ],
                ),
                SizedBox(
                  height: 15,
                ),
                Row(
                  children: [
                    Expanded(
                      child: _quantityIncremented(),
                    ),
                    Text(
                      "\$ 200",
                      style:
                      TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
                    )
                  ],
                )
              ],
            ),
          ),
          // SizedBox(
          //   width: 42,
          // ),
        ],
      ),
    );
  }

  _quantityIncremented() {
    return Container(
      child: Row(
        children: [
          Container(
            height: 31,
            width: 31,
            decoration: BoxDecoration(
                border: Border.all(color: Color(0xffD9D9D9)),
                borderRadius: BorderRadius.circular(9.14)),
            padding: EdgeInsets.symmetric(vertical: 15,horizontal: 8),
            child: InkWell(
              onTap: () {
                // if (qty.qty > 1) {
                // qty.qty = --qty.qty;
                // removefromCart(serviceId: qty.purchasedId);
                // getCart();
                // }

                setState(() {});
              },
              child: Icon(
                Icons.horizontal_rule_outlined,
                size: 15,
                color: AppColors.appColor,
              ),
            ),
          ),
          SizedBox(
            width: 11,
          ),
          Container(
            // decoration: BoxDecoration(
            //   color: Colors.white,
            //   border: Border.all(
            //     color: Color(0xffC2DCD4),
            //   ),
            // ),
            // padding: EdgeInsets.symmetric(horizontal: 12, vertical: 3),
              child: Text(
                "2",
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
              )),
          SizedBox(
            width: 11,
          ),
          Container(
            height: 31,
            width: 31,
            decoration: BoxDecoration(
                border: Border.all(color: Color(0xffD9D9D9)),
                borderRadius: BorderRadius.circular(9.14)),
            child: InkWell(
                onTap: () {
                  setState(
                        () {
                      // qty.qty = qty.qty + 1;
                      // addtocartapi(serviceId: qty.servicesId, price: qty.price);
                    },
                  );
                },
                child: Icon(
                  Icons.add,
                  color: AppColors.appColorLight,
                  size: 15,
                )),
          ),
        ],
      ),
    );
  }

  _totalView() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 23),
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.symmetric(vertical: 10),
            width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                  color:Colors.grey[300],

                  borderRadius: BorderRadius.circular(10)),
              child:
            Text("Apply Coupon",textAlign: TextAlign.center,)),
SizedBox(height:10),
          Row(
            children: [
              Expanded(
                  child: Text(
                    "Items",
                    style: TextStyle(
                        fontSize: 14.76,
                        color: Color(0xff16162E),
                        fontWeight: FontWeight.w500),
                  )),
              Text(
                "1",
                style: TextStyle(
                    fontSize: 14,
                    color: Colors.black,
                    fontWeight: FontWeight.bold),
              )
            ],
          ),
          SizedBox(height:5),
          Row(
            children: [
              Expanded(
                  child: Text(
                    "Price",
                    style: TextStyle(
                        fontSize: 14.76,
                        color: Color(0xff16162E),
                        fontWeight: FontWeight.w500),
                  )),
              Text(
                "₹ 230.00",
                style: TextStyle(
                    fontSize: 14,
                    color: Colors.black,
                    fontWeight: FontWeight.bold),
              )
            ],
          ),

          SizedBox(height:5),
          Row(
            children: [
              Expanded(
                  child: Text(
                    "GST",
                    style: TextStyle(
                        fontSize: 14.76,
                        color: Color(0xff16162E),
                        fontWeight: FontWeight.w500),
                  )),
              Text(
                "₹ 30.25",
                style: TextStyle(
                    fontSize: 14,
                    color: Colors.black,
                    fontWeight: FontWeight.bold),
              )
            ],
          ),

          SizedBox(height:5),
          Container(
            margin: EdgeInsets.symmetric(vertical: 8),

            color: Colors.grey,
            height: 1.0,

          ),
          SizedBox(height:5),
          Row(
            children: [
              Expanded(
                  child: Text(
                    "Total price:",
                    style: TextStyle(
                        fontSize: 14.76,
                        color: Color(0xff16162E),
                        fontWeight: FontWeight.w500),
                  )),
              Text(
                "₹ 250.00",
                style: TextStyle(
                    fontSize: 14,
                    color: Colors.black,
                    fontWeight: FontWeight.bold),
              )
            ],
          ),
        ],
      ),
    );
  }

  _bottomButton() {
    return Container(
      margin:EdgeInsets.symmetric(horizontal: 17),
      child: MaterialButton(
        color: AppColors.appColor,
        padding: EdgeInsets.symmetric(vertical: 10),
        minWidth: MediaQuery.of(context).size.width,
          onPressed: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => BottomNavigationBarView(selectedIndex: 0)));
          },
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          child: Text("Buy",style: TextStyle(color:Colors.white,fontSize: 18),),
          ),
    );
  }
}
