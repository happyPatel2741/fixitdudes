import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fixeditdudes/Constants/appColors.dart';
import 'package:fixeditdudes/Data/data.dart';
import '../course_info_screen.dart';

Widget userList(BuildContext context, int index) {
  return Stack(
    alignment: Alignment.center,
    children: <Widget>[
      Padding(
        padding: const EdgeInsets.fromLTRB(50, 10, 10, 5),
        child: InkWell(
          onTap: () {

          },
          child: Container(
            padding: EdgeInsets.only(left: 55, top: 10),
            decoration: BoxDecoration(
              color: Colors.white70,
              borderRadius: BorderRadius.all(Radius.circular(10)),
            ),
            width: double.infinity,
            height: 115,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        details[index]['name'],
                        style: TextStyle(
                            color: AppColors.textColor,
                            fontWeight: FontWeight.bold,
                            fontSize: 16),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Text(
                          "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                              color: AppColors.textColor,
                              fontSize: 13,
                              letterSpacing: .3)),
                      SizedBox(
                        height: 5,
                      ),
                      Text(
                        details[index]['rate'],
                        style: TextStyle(
                            color: AppColors.textColor,
                            fontSize: 13,
                            letterSpacing: .3),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Text(details[index]['time'],
                          style: TextStyle(
                              color: AppColors.textColor,
                              fontSize: 13,
                              letterSpacing: .3)),
                    ],
                  ),
                ),
                SizedBox(
                  width: 10,
                ),
                MaterialButton(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5)),
                  child: Text(
                    "Add",
                    style: TextStyle(color: Colors.white),
                  ),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => CourseInfoScreen()),
                    );
                  },
                  color: AppColors.appColor,
                  minWidth: 40,
                ),
                SizedBox(
                  width: 10,
                ),
              ],
            ),
          ),
        ),
      ),
      Positioned(
        left: 10,
        child: Container(
          margin: EdgeInsets.only(left: 10),
          width: 70,
          height: 70,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(5)),
            image: DecorationImage(
              image: NetworkImage('https://picsum.photos/seed/picsum/200/300'),
              fit: BoxFit.fill,
            ),
          ),
        ),
      ),
    ],
  );
}
