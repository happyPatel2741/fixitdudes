import 'package:custom_navigation_bar/custom_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fixeditdudes/AppScreenView/ProfileScreen.dart';
import 'package:fixeditdudes/AppScreenView/SearchScreen.dart';
import 'package:fixeditdudes/AppScreenView/HistoryScreen.dart';
import 'package:fixeditdudes/Constants/appColors.dart';
import 'package:fixeditdudes/pages/my_diary_screen.dart';

class BottomNavigationBarView extends StatefulWidget {
  int selectedIndex;

  BottomNavigationBarView({@required this.selectedIndex});

  @override
  _BottomNavigationBarViewState createState() =>
      _BottomNavigationBarViewState();
}

class _BottomNavigationBarViewState extends State<BottomNavigationBarView> {
  Widget _widget = MyDiaryScreen();
  int _selectedIndex = 0;
  GlobalKey _bottomNavigationKey = GlobalKey();

  void _onItemTapped(int index) {
    if (mounted)
      setState(() {
        _selectedIndex = index;
      });
    switch (index) {
      case 0:
        changewidget(MyDiaryScreen());
        break;
      case 1:
        changewidget(SearchScreen());
        break;
      case 2:
        changewidget(HistoryScreen());
        break;
      case 3:
        changewidget(ProfileScreen());
        break;
      case 4:
        changewidget(MyDiaryScreen());
        break;
    }
  }

  changewidget(Widget widget) {
    if (mounted) {
      setState(() {
        _widget = widget;
      });
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _selectedIndex = widget.selectedIndex;
    _onItemTapped(_selectedIndex);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: AnnotatedRegion(
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Expanded(
                child: _widget,
              ),
            ],
          ),
        ),
        value: SystemUiOverlayStyle(
            statusBarColor: Colors.transparent,
            statusBarIconBrightness: Brightness.light),
      ),
      bottomNavigationBar: _bottomBar2(),
    );
  }

  _bottomBar() {
    return Container(
      child: BottomNavigationBar(
        elevation: 0,
        currentIndex: _selectedIndex,
        onTap: _onItemTapped,
        backgroundColor: Colors.white,
        type: BottomNavigationBarType.fixed,
        unselectedLabelStyle: TextStyle(
            color: AppColors.appColor,
            fontSize: 11,
            fontWeight: FontWeight.w500),
        selectedLabelStyle: TextStyle(
            color: Colors.black, fontSize: 11, fontWeight: FontWeight.w500),
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            label: "",
            activeIcon: Image.asset(
              "assets/images/Home.png",
              width: 27,
              height: 25,
            ),
            icon: Image.asset(
              "assets/images/Home.png",
              width: 27,
              height: 25,
              color: Color(0xff707070),
            ),
          ),
          BottomNavigationBarItem(
            label: "",
            icon: Image.asset(
              "assets/images/ticket.png",
              width: 27,
              height: 25,
            ),
            activeIcon: Image.asset(
              "assets/images/ticket.png",
              width: 27,
              height: 25,
              color: AppColors.appColor,
            ),
          ),
          BottomNavigationBarItem(
            label: "",
            icon: Image.asset(
              "assets/images/activity.png",
              width: 27,
              height: 25,
            ),
            activeIcon: Image.asset(
              "assets/images/activity.png",
              width: 27,
              height: 25,
              color: AppColors.appColor,
            ),
          ),
          BottomNavigationBarItem(
            label: "",
            icon: Image.asset(
              "assets/images/user.png",
              width: 27,
              height: 25,
            ),
            activeIcon: Image.asset(
              "assets/images/user.png",
              width: 27,
              height: 25,
              color: AppColors.appColor,
            ),
          ),
        ],
      ),
    );
  }

  _bottomBar2() {
    return Padding(
      padding: const EdgeInsets.only(bottom: 8.0),
      child: CustomNavigationBar(
        iconSize: 30.0,
        borderRadius: Radius.circular(10),
        selectedColor: Colors.white,
        strokeColor: Colors.white38,
        unSelectedColor: Colors.white60,
        backgroundColor: AppColors.appColor,
        isFloating: true,
        items: [
          CustomNavigationBarItem(
            icon: Icon(Icons.home),
          ),
          CustomNavigationBarItem(
            icon: Icon(Icons.search),
          ),
          CustomNavigationBarItem(
            icon: Icon(Icons.history),
          ),
          CustomNavigationBarItem(
            icon: Icon(Icons.account_circle),
          ),
        ],

        currentIndex: _selectedIndex,
        onTap: _onItemTapped,
      ),
    );
  }
}
