import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fixeditdudes/Constants/appColors.dart';
import 'package:fixeditdudes/bottom_nav/bottomNavigationBarHome.dart';
import 'package:fixeditdudes/pages/home_page.dart';

class EnterUserDetail extends StatefulWidget {
  @override
  _EnterUserDetailState createState() => _EnterUserDetailState();
}

class _EnterUserDetailState extends State<EnterUserDetail> {
  bool _obscureText = true;
  // String _password;
  TextEditingController textFieldController1 = new TextEditingController();
  TextEditingController textFieldController2 = new TextEditingController();
  TextEditingController textFieldController3 = new TextEditingController();

  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  method(String hintText, Icon icon, TextEditingController controller,
      bool _obscuretext2, int maxLine) {
    return TextField(
      maxLines: maxLine,
      controller: controller,
      obscureText: _obscuretext2,
      onChanged: (value) {
        print(value);
      },
      decoration: InputDecoration(
          hintText: hintText,
          prefixIcon: icon,
          border: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
          )),
      onTap: _toggle,
    );
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,));
    return Scaffold(
      appBar: AppBar(title:Text("User Details",style: TextStyle(color: Colors.black,fontWeight: FontWeight.w600,fontSize: 22),)
      ,automaticallyImplyLeading: false,
       backgroundColor: Colors.transparent,
       centerTitle: true,
        elevation: 0,
      ),
      body:
      AnnotatedRegion<SystemUiOverlayStyle>(

          value: SystemUiOverlayStyle.dark, child: _body()),
    );
  }

  _body() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20),
      child: SingleChildScrollView(
        child: Column(children: [
          SizedBox(
            height: 15,
          ),
          method(
              "FULL NAME",
              Icon(
                Icons.person,
                color: AppColors.appColor,
              ),
              textFieldController1,
              false,
              1),
          SizedBox(
            height: 15,
          ),
          method(
              "LANDMARK", Icon(Icons.email,                color: AppColors.appColor,
          ), textFieldController2, false, 1),
          SizedBox(
            height: 15,
          ),
          method("PINCODE", Icon(Icons.visibility, color: AppColors.appColor),
              textFieldController3, false, 1),
          SizedBox(
            height: 15,
          ),
          method(
              "ALTERNATIVE CONTACT NUMBER",
              Icon(Icons.call, color: AppColors.appColor),
              textFieldController2,
              false,
              1),
          SizedBox(
            height: 15,
          ),
          method("REFERED BY", Icon(Icons.email, color: AppColors.appColor),
              textFieldController2, false, 1),
          SizedBox(
            height: 15,
          ),
          method(
              "ADDRESS",
              Icon(Icons.location_city, color: AppColors.appColor),
              textFieldController2,
              false,
              3),
          SizedBox(
            height: 15,
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(0, 20, 0, 0),
            child: SizedBox(
              width: 300,
              height: 40,
              child: FlatButton(
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => BottomNavigationBarView(
                        selectedIndex: 0,
                      )));
                },
                color: AppColors.appColorLight,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                child: Text(
                  'Sign in',
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ),
          ),
          // Padding(
          //   padding: const EdgeInsets.fromLTRB(0, 20, 0, 0),
          //   child: SizedBox(
          //     width: 300,
          //     height: 40,
          //     child: FlatButton(
          //       onPressed: () {},
          //       color: AppColors.appColorLight,
          //       shape: RoundedRectangleBorder(
          //         borderRadius: BorderRadius.circular(10.0),
          //       ),
          //       child: Text(
          //         'Sign Up with Facebook',
          //         style: TextStyle(color: Colors.white),
          //       ),
          //     ),
          //   ),
          // ),
        ]),
      ),

      decoration: BoxDecoration(
          color: Colors.white,
          border: Border.all(
            color: Colors.white,
          ),
          borderRadius: BorderRadius.circular(10)),
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      // color: Colors.white,
    );
  }
}
